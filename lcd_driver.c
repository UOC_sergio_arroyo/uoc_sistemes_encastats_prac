/*
 * lcd_driver.c
 *
 *  Created on: 11 dic. 2017
 *      Author: sarroyo
 */

#include <driverlib.h>
#include <grlib.h>
#include "Crystalfontz128x128_ST7735.h"
#include "HAL_MSP_EXP432P401R_Crystalfontz128x128_ST7735.h"
#include "i2c_driver.h"
#include "lcd_driver.h"

// Contexto de configuracion para la pantalla
Graphics_Context g_sContext;


// Metodo de inicializacion de la pantalla LCD
void Init_LCD(void) {
    // Inicializa el display
    Crystalfontz128x128_Init();

    // Orientacion de la pantalla
    Crystalfontz128x128_SetOrientation(LCD_ORIENTATION_UP);

    /* Initializes graphics context */
    Graphics_initContext(&g_sContext, &g_sCrystalfontz128x128);
    Graphics_setForegroundColor(&g_sContext, LCD_FOREGROUND_LABEL_COLOR);
    Graphics_setBackgroundColor(&g_sContext, LCD_BACKGROUND_COLOR);
    GrContextFontSet(&g_sContext, &g_sFontFixed6x8);
    Graphics_clearDisplay(&g_sContext);

    // Inicializa la comunicacion I2C
    Init_I2C_GPIO();
}

// Imprime una cadena de texto por pantalla
void print_LCD(char *s, int32_t x, int32_t y, const int32_t fgColor)
{
    static int32_t prevFgColor = -1;
    char string[70];

    sprintf(string, "%s", s);
    if (prevFgColor != fgColor) {
        Graphics_setForegroundColor(&g_sContext, fgColor);
        prevFgColor = fgColor;
    }
    Graphics_drawString(&g_sContext,
                        string,
                        AUTO_STRING_LENGTH,
                        x,
                        y,
                        OPAQUE_TEXT);
}

