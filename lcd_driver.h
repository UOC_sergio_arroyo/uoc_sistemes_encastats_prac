/*
 * lcd_driver.h
 *
 *  Created on: 11 dic. 2017
 *      Author: sarroyo
 */

#ifndef LCD_DRIVER_H_
#define LCD_DRIVER_H_

#include "grlib.h"

#define LCD_BACKGROUND_COLOR             GRAPHICS_COLOR_BLACK
#define LCD_FOREGROUND_LABEL_COLOR       GRAPHICS_COLOR_GREEN
#define LCD_FOREGROUND_VALUE_COLOR       GRAPHICS_COLOR_GREEN_YELLOW
#define LCD_FG_VALUE_POSITIVE_COLOR      GRAPHICS_COLOR_BLUE
#define LCD_FG_VALUE_NEGATIVE_COLOR      GRAPHICS_COLOR_RED

// Metodo de inicializacion de la pantalla LCD
void Init_LCD(void);

// Imprime una cadena de texto por pantalla
void print_LCD(char *s, int32_t x, int32_t y, const int32_t fgColor);


#endif /* LCD_DRIVER_H_ */
