//*****************************************************************************
//
// Copyright (C) 2015 - 2016 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//
//  Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the
//  distribution.
//
//  Neither the name of Texas Instruments Incorporated nor the names of
//  its contributors may be used to endorse or promote products derived
//  from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

// Includes standard
#include <stdio.h>
#include <stdint.h>
#include <string.h>

// Includes FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

// Includes drivers
#include "i2c_driver.h"
#include "temp_sensor.h"
#include "opt3001.h"
#include "tmp006.h"

#include "accelerometer_driver.h"
#include "adc14_multiple_channel_no_repeat.h"

#include "general_functions.h"

#include "lcd_driver.h"


// Definicion de prioridades en las tareas FreeRTOS
#define HEARTBEAT_TASK_PRIORITY           1
#define TEMPLIGHT_WRITER_TASK_PRIORITY    3
#define ACCELEROM_WRITER_TASK_PRIORITY    3
#define READER_TASK_PRIORITY              2

// Numero maximo de valores que podra tener cada una de las colas
#define TEMP_LIGHT_QUEUE_SIZE             10
#define ACCELERATOR_QUEUE_SIZE            10

// Intervalos de ejecucion de las tareas
#define HEART_BEAT_INTERVAL_ON_MS         10
#define HEART_BEAT_INTERVAL_OFF_MS        990
#define TEMP_LIGHT_SENSORS_MS             500
#define ACCELEROM_SENSORS_MS              100
#define QUEUES_READER_MS                  50

// Intervalos de actualizacion en pantalla cada N numero de muestras
#define LCD_WRITE_ACCEL_EVERY_SMPL        10
#define LCD_WRITE_LIGHT_EVERY_SMPL        2
#define LCD_WRITE_TEMP_EVERY_SMPL         60
#define READ_ACCEL_TEMP_EVERY             30
#define REFRESH_SCREEN_EVERY              10


// Indica si queremos enviar mensajes de debug por UART
#define DEBUG_MSG_BY_UART                 1


// UART Configuration Parameter (9600 bps - clock 8MHz)
const eUSCI_UART_Config uartConfig =
{
        EUSCI_A_UART_CLOCKSOURCE_SMCLK,                // SMCLK Clock Source
        52,                                            // BRDIV = 52
        1,                                             // UCxBRF = 1
        0,                                             // UCxBRS = 0
        EUSCI_A_UART_NO_PARITY,                        // No Parity
        EUSCI_A_UART_LSB_FIRST,                        // LSB First
        EUSCI_A_UART_ONE_STOP_BIT,                     // One stop bit
        EUSCI_A_UART_MODE,                             // UART mode
        EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION  // Oversampling
};

// Definicion de los tipos enumerados
typedef enum Sensor {
    light = 1,
    temp  = 2
} Sensor;

// Tipo de estructuras de almacenamiento de datos para guardar en las colas
typedef struct {
    Sensor sensor;
    float value;
} TempLight_reg_t;

typedef struct {
    float acc_X;
    float acc_Y;
    float acc_Z;
} Accelerom_reg_t;

// Prototipos de funciones privadas
static void prvSetupHardware(void);
static void prvHeartBeatTask(void *pvParameters);
static void prvTempLightWriterTask(void *pvParameters);
static void prvAcceleratorWriterTask(void *pvParameters);
static void prvReaderQueuesTask(void *pvParameters);

void printLabels_LCD();
void printAccel_LCD(Accelerom_reg_t *accelData);
void printLight_LCD(float *lightVal);
void printTemp_LCD(float *tempVal);

float getTemperatureValue(void);
float getLightValue(void);
void printf_UART(char *message, bool printNL);

// Declaracion de un semaforo binario
volatile SemaphoreHandle_t xBinarySemaphore;

// Declaracion de un mutex para acceso unico a I2C, UART i Timer general
SemaphoreHandle_t xMutexI2C;
SemaphoreHandle_t xMutexUART;
SemaphoreHandle_t xMutexGlobalTemp;

// Declaracion de las colas implementadas por FreeRTOS
QueueHandle_t xQueueTempLight;
QueueHandle_t xQueueAccelerator;

// Contenedor de valor medio para la temperatura
float globalAvgTemp = 20.0f;


int main(void)
{
    // Inicializacion de las colas necesarias
    xQueueTempLight   = xQueueCreate(TEMP_LIGHT_QUEUE_SIZE, sizeof(TempLight_reg_t));
    xQueueAccelerator = xQueueCreate(ACCELERATOR_QUEUE_SIZE, sizeof(Accelerom_reg_t));

    // Inicializacio de mutexs
    xMutexI2C  = xSemaphoreCreateMutex();
    xMutexUART = xSemaphoreCreateMutex();
    xMutexGlobalTemp = xSemaphoreCreateMutex();

    // Inicializacion del semaforo binario para ADC
    xBinarySemaphore = xSemaphoreCreateBinary();

    // Comprueba si semaforo y mutex se han creado bien
    if ((xMutexI2C != NULL) && (xMutexUART != NULL) && (xBinarySemaphore != NULL) &&
            (xMutexGlobalTemp != NULL))
    {
        // Inicializacion del hardware (clocks, GPIOs, IRQs)
        prvSetupHardware();

        // Creacion de tareas
        // Tarea 1: Heartbeat
        xTaskCreate( prvHeartBeatTask, "HeartBeatTask",
                     configMINIMAL_STACK_SIZE, NULL,
                     HEARTBEAT_TASK_PRIORITY, NULL );

        // Tarea 2: Lectura luz y temperatura e insercion en cola
        xTaskCreate( prvTempLightWriterTask, "TempLightWriterTask",
                     configMINIMAL_STACK_SIZE, NULL,
                     TEMPLIGHT_WRITER_TASK_PRIORITY, NULL );

        // Tarea 3: Lectrura acelerometro e insercion en cola
        xTaskCreate( prvAcceleratorWriterTask, "AcceleratorWriterTask",
                     configMINIMAL_STACK_SIZE, NULL,
                     ACCELEROM_WRITER_TASK_PRIORITY, NULL );

        // Tarea 4: Lectura de colas y presentacion de valores por pantalla
        xTaskCreate( prvReaderQueuesTask, "ReaderQueuesTask",
                     configMINIMAL_STACK_SIZE * 3, NULL,
                     READER_TASK_PRIORITY, NULL );

        // Puesta en marcha de las tareas creadas
        if (DEBUG_MSG_BY_UART) printf_UART("Tareas creadas", true);
        vTaskStartScheduler();
    }

    // Solo llega aqui si no hay suficiente memoria
    // para iniciar el scheduler
    return 0;
}

// Inicializacion del hardware del sistema
static void prvSetupHardware(void)
{
    extern void FPU_enableModule(void);

    Init_LCD();

    // Configuracion del pin P1.0 - LED 1 - como salida y puesta en OFF
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);

    // Inicializacion de pins sobrantes para reducir consumo
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PB, PIN_ALL16);
    //MAP_GPIO_setAsOutputPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PE, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PB, PIN_ALL16);
    //MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PE, PIN_ALL16);

    // Habilita la FPU
    MAP_FPU_enableModule();

    // Cambia el numero de "wait states" del controlador de Flash
    MAP_FlashCtl_setWaitState(FLASH_BANK0, 2);
    MAP_FlashCtl_setWaitState(FLASH_BANK1, 2);

    // Selecciona la frecuencia central de un rango de frecuencias del DCO
    MAP_CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_6);

    // Configura la frecuencia del DCO
    CS_setDCOFrequency(CS_8MHZ);

    // Inicializa los clocks HSMCLK, SMCLK, MCLK y ACLK
    MAP_CS_initClockSignal(CS_HSMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_ACLK, CS_REFOCLK_SELECT, CS_CLOCK_DIVIDER_1);

    // Selecciona el nivel de tension del core
    MAP_PCM_setCoreVoltageLevel(PCM_VCORE0);

    // Inicializacion del I2C
    I2C_init();

    // Inicializacion del sensor TMP006
    TMP006_init();

    // Inicializacion del sensor opt3001
    sensorOpt3001Init();
    sensorOpt3001Enable(true);

    // Seleccion de modo UART en pines P1.2 y P1.3
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1,
                                                   GPIO_PIN2 | GPIO_PIN3,
                                                   GPIO_PRIMARY_MODULE_FUNCTION);

    // Configuracion de la UART
    MAP_UART_initModule(EUSCI_A0_BASE, &uartConfig);
    // Habilitacion de la UART
    MAP_UART_enableModule(EUSCI_A0_BASE);

    // Inicializa acelerometro+ADC
    init_Accel();

    // Configura la prioridad de la interrupcion del ADC14
    //MAP_Interrupt_setPriority(INT_ADC14, 0xA0);

    // Habilita que el procesador responda a las interrupciones
    MAP_Interrupt_enableMaster();
}

// Tarea 1: Heartbeat del led rojo pin 1.0
static void prvHeartBeatTask(void *pvParameters)
{
    // Definicion del numero de ticks a esperar por cada intervalo
    static TickType_t xPeriod_ON  = pdMS_TO_TICKS(HEART_BEAT_INTERVAL_ON_MS);
    static TickType_t xPeriod_OFF = pdMS_TO_TICKS(HEART_BEAT_INTERVAL_OFF_MS);

    for (;;) {
        // Led ON
        MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay(xPeriod_ON);

        // Led OFF
        MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay(xPeriod_OFF);
    }
}


// Tarea 2: Lectura de las se�ales de temperatura y luz para posteriormente
// introducirlas en la cola
static void prvTempLightWriterTask(void *pvParameters)
{
    // Definicion del numero de ticks a esperar por cada intervalo
    static TickType_t xPeriodSensors = pdMS_TO_TICKS(TEMP_LIGHT_SENSORS_MS);

    // Registro de cola para las lecturas tomadas
    TempLight_reg_t queueReg;

    // Inidicador del tipo de lectura a tomar:
    // 0  = solo sensor luminosidad
    // !0 = ambos sensores (luminosidad y temperatura)
    int readBothSensors = 0;

    for (;;) {
        // Realiza la lectura de uno o ambos sensores en funcion de readBothSensors:
        queueReg.sensor = light;
        queueReg.value  = getLightValue();

        // Introduce en la cola el valor leido de luz
        xQueueSend(xQueueTempLight, &queueReg, portMAX_DELAY);

        if (readBothSensors) {
            queueReg.sensor = temp;
            queueReg.value  = getTemperatureValue();

            // Introduce en la cola el valor leido de temperatura
            xQueueSend(xQueueTempLight, &queueReg, portMAX_DELAY);
        }

        // Invierte el valor de la variable
        readBothSensors = !readBothSensors;

        // Envia un comando a traves de la cola si hay espacio
        if (DEBUG_MSG_BY_UART && xSemaphoreTake(xMutexUART, portMAX_DELAY)) {
            printf_UART("Enviando temp/luz..", true);
            xSemaphoreGive(xMutexUART);
        }

        // Espera el tiempo fijado para esta tarea
        vTaskDelay(xPeriodSensors);
    }
}


// Tarea 3: Lectura de las se�ales de acelerometro para posteriormente
// introducirlas en la cola
static void prvAcceleratorWriterTask(void *pvParameters)
{
    // Definicion del numero de ticks a esperar por cada intervalo
    static TickType_t xPeriodSensors = pdMS_TO_TICKS(ACCELEROM_SENSORS_MS);

    float Datos[NUM_ADC_CHANNELS];
    Accelerom_reg_t queueReg;

    uint16_t readGlobalTempEvery = READ_ACCEL_TEMP_EVERY * (1000/ACCELEROM_SENSORS_MS);
    uint16_t nCicles = readGlobalTempEvery;
    float localAvgTemp;

    xSemaphoreTake(xBinarySemaphore, 0);
    for (;;) {

        // Actualiza el valor de temperatura media
        if ((nCicles >= readGlobalTempEvery)
                && xSemaphoreTake(xMutexGlobalTemp, portMAX_DELAY))
        {
            localAvgTemp = globalAvgTemp;
            xSemaphoreGive(xMutexGlobalTemp);

            nCicles = 0;
        }

        // Lee los valores del acelerometro
        Accel_read(Datos, localAvgTemp);
        queueReg.acc_X = Datos[0];
        queueReg.acc_Y = Datos[1];
        queueReg.acc_Z = Datos[2];

        // Introduce en la cola el valor leido de luz
        xQueueSend(xQueueAccelerator, &queueReg, portMAX_DELAY);

        // Envia un comando a traves de la cola si hay espacio
        if (DEBUG_MSG_BY_UART && xSemaphoreTake(xMutexUART, portMAX_DELAY)) {
            printf_UART("X: ", true);
            xSemaphoreGive(xMutexUART);
        }

        nCicles++;

        // Espera el tiempo fijado para esta tarea
        vTaskDelay(xPeriodSensors);
    }
}


// Tarea 4: Encargada de realizar la lectura de las colas
static void prvReaderQueuesTask(void *pvParameters)
{
    // Definicion del numero de ticks a esperar por cada intervalo
    static TickType_t xPeriodSensors = pdMS_TO_TICKS(QUEUES_READER_MS);
    TempLight_reg_t qTempLightReg;
    Accelerom_reg_t qAcceleromReg;

    // Valores locales de los valores medios para el acelerometro,
    // luz y temperatura
    Accelerom_reg_t localAvgAccel = {0.0f, 0.0f, 1.0f};
    float localAvgLight = 0.0f;
    float localAvgTemp  = 0.0f;

    // Control del numero de muestras recibidas de cada tipo para
    // realizar el refresco de forma selectiva de cada una de ellas
    uint8_t nAccelSamples = 0;
    uint8_t nLightSamples = 0;
    uint8_t nTempSamples  = 0;

    // Imprime las labels una sola vez antes de iniciar el buble infinito
    printLabels_LCD();

    for (;;) {
        // ACCEL: Recibe los registros del acelerometro disponibles en la cola
        if (xQueueReceive(xQueueAccelerator, &qAcceleromReg, 5) == pdPASS) {
            localAvgAccel.acc_X = ((0.9*localAvgAccel.acc_X) + (0.1*qAcceleromReg.acc_X));
            localAvgAccel.acc_Y = ((0.9*localAvgAccel.acc_Y) + (0.1*qAcceleromReg.acc_Y));
            localAvgAccel.acc_Z = ((0.9*localAvgAccel.acc_Z) + (0.1*qAcceleromReg.acc_Z));

            if (++nAccelSamples >= LCD_WRITE_ACCEL_EVERY_SMPL) {
                // Muestra los datos de acelerador por pantalla
                printAccel_LCD(&localAvgAccel);

                nAccelSamples = 0;
            }
        }

        // LIGHT & TEMP: Recibe los registros de luz y temperatura disponibles
        // en la cola y a�ade los valores leidos en la media local
        if (xQueueReceive(xQueueTempLight, &qTempLightReg, 5) == pdPASS) {
            if (qTempLightReg.sensor == light) {
                localAvgLight = ((0.5*localAvgLight) + (0.5*qTempLightReg.value));

                if (++nLightSamples >= LCD_WRITE_LIGHT_EVERY_SMPL) {
                    // Muestra los datos de luz por pantalla
                    printLight_LCD(&localAvgLight);

                    nLightSamples = 0;
                }
            }
            else
            if (qTempLightReg.sensor == temp) {
                localAvgTemp += qTempLightReg.value;

                // Actualiza el valor de temperatura global
                if ((++nTempSamples >= LCD_WRITE_TEMP_EVERY_SMPL)
                        && xSemaphoreTake(xMutexGlobalTemp, portMAX_DELAY))
                {
                    localAvgTemp /= 60;
                    globalAvgTemp = localAvgTemp;
                    xSemaphoreGive(xMutexGlobalTemp);

                    // Muestra los datos de temperatura por pantalla
                    printTemp_LCD(&localAvgTemp);

                    localAvgTemp = 0.0f;
                    nTempSamples = 0;
                }
            }
        }

        // Espera el tiempo fijado para esta tarea
        vTaskDelay(xPeriodSensors);
    }
}

// Imprime por pantalla las etiquetas. Esto solo se realizara una
// vez al inicio de la ejecucion
void printLabels_LCD()
{
    const uint8_t yEqualPos = 60;

    // Etiquetas de acelerometro
    print_LCD("Accel:", 10, 20, LCD_FOREGROUND_LABEL_COLOR);
    print_LCD("X", 30, 30, LCD_FOREGROUND_LABEL_COLOR);
    print_LCD("=", yEqualPos, 30, LCD_FOREGROUND_LABEL_COLOR);
    print_LCD("Y", 30, 40, LCD_FOREGROUND_LABEL_COLOR);
    print_LCD("=", yEqualPos, 40, LCD_FOREGROUND_LABEL_COLOR);
    print_LCD("Z", 30, 50, LCD_FOREGROUND_LABEL_COLOR);
    print_LCD("=", yEqualPos, 50, LCD_FOREGROUND_LABEL_COLOR);

    // Etiquetas de luz
    print_LCD("Light", 10, 70, LCD_FOREGROUND_LABEL_COLOR);
    print_LCD("=", yEqualPos, 70, LCD_FOREGROUND_LABEL_COLOR);

    // Etiquetas de tempetarura
    print_LCD("Temp:", 10, 90, LCD_FOREGROUND_LABEL_COLOR);
    print_LCD("Act", 30, 100, LCD_FOREGROUND_LABEL_COLOR);
    print_LCD("=", yEqualPos, 100, LCD_FOREGROUND_LABEL_COLOR);
    print_LCD("Diff", 30, 110, LCD_FOREGROUND_LABEL_COLOR);
    print_LCD("=", yEqualPos, 110, LCD_FOREGROUND_LABEL_COLOR);
}

// Imprime por pantalla los datos del acelerometro
void printAccel_LCD(Accelerom_reg_t *accelData)
{
    static Accelerom_reg_t prevAccelVal = {-99999, -99999, -99999};
    const uint8_t yValuesPos = 75;
    char value[10];

    // Eje X
    if (accelData->acc_X != prevAccelVal.acc_X) {
        ftoa(accelData->acc_X, value, 2);
        strcat(value, " ");
        print_LCD(value, yValuesPos, 30, LCD_FOREGROUND_VALUE_COLOR);
        prevAccelVal.acc_X = accelData->acc_X;
    }

    // Eje Y
    if (accelData->acc_Y != prevAccelVal.acc_Y) {
        ftoa(accelData->acc_Y, value, 2);
        strcat(value, " ");
        print_LCD(value, yValuesPos, 40, LCD_FOREGROUND_VALUE_COLOR);
        prevAccelVal.acc_Y = accelData->acc_Y;
    }

    // Eje Z
    if (accelData->acc_Z != prevAccelVal.acc_Z) {
        ftoa(accelData->acc_Z, value, 2);
        strcat(value, " ");
        print_LCD(value, yValuesPos, 50, LCD_FOREGROUND_VALUE_COLOR);
        prevAccelVal.acc_Z = accelData->acc_Z;
    }
}

// Imprime por pantalla el valor de luz
void printLight_LCD(float *lightVal)
{
    static float prevLightVal = -99999;
    const uint8_t yValuesPos = 75;
    char value[10];

    // Valor de luz
    if (*lightVal != prevLightVal) {
        ftoa(*lightVal, value, 2);
        strcat(value, " ");
        print_LCD(value, yValuesPos, 70, LCD_FOREGROUND_VALUE_COLOR);
        prevLightVal = *lightVal;
    }
}

// Imprime por pantalla el valor de temperatura
void printTemp_LCD(float *tempVal)
{
    static float prevAvgTemp = -99999;
    const uint8_t yValuesPos = 75;
    char value[10];
    float diffTemp;

    // Valores de temperatura
    ftoa(*tempVal, value, 2);
    strcat(value, " C ");
    print_LCD(value, yValuesPos, 100, LCD_FOREGROUND_VALUE_COLOR);

    if (prevAvgTemp > -99999) {
        diffTemp = *tempVal-prevAvgTemp;

        ftoa(diffTemp, value, 2);
        strcat(value, " C ");
        if (diffTemp > 0.0)
            print_LCD(value, yValuesPos, 110, LCD_FG_VALUE_POSITIVE_COLOR);
        else
            print_LCD(value, yValuesPos, 110, LCD_FG_VALUE_NEGATIVE_COLOR);
    }

    // Se actualiza la temperatura previa con la actual
    prevAvgTemp = *tempVal;
}

// Realiza la lectura del sensor de luz y devuleve su valor
float getTemperatureValue(void)
{
    // Lee el valor de la medida de temperatura
    return TMP006_readAmbientTemperature();
}

// Realiza la lectura del sensor de luz y devuleve su valor
float getLightValue(void)
{
    uint16_t rawData;
    float convertedLux;

    // Lee el valor de la medida de luz
    sensorOpt3001Read(&rawData);
    sensorOpt3001Convert(rawData, &convertedLux);

    return convertedLux;
}

// Funcion encargada de enviar una cadena por UART
void printf_UART(char *message, bool printNL)
{
    for (int i=0; message[i]!='\0'; i++)
        MAP_UART_transmitData(EUSCI_A0_BASE, message[i]);

    if (printNL)
        MAP_UART_transmitData(EUSCI_A0_BASE, '\n');
}

