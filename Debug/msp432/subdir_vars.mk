################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/adc14.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/aes256.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/comp_e.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/cpu.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/crc32.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/cs.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/dma.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/flash.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/fpu.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/gpio.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/i2c.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/interrupt.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/mpu.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/pcm.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/pmap.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/pss.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/ref_a.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/reset.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/rtc_c.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/spi.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/sysctl.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/systick.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/timer32.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/timer_a.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/uart.c \
D:/UOC_SOFTWARE/Sistemes\ Encastats/workspace_v7/Libraries/ti/msp432/wdt_a.c 

C_DEPS += \
./msp432/adc14.d \
./msp432/aes256.d \
./msp432/comp_e.d \
./msp432/cpu.d \
./msp432/crc32.d \
./msp432/cs.d \
./msp432/dma.d \
./msp432/flash.d \
./msp432/fpu.d \
./msp432/gpio.d \
./msp432/i2c.d \
./msp432/interrupt.d \
./msp432/mpu.d \
./msp432/pcm.d \
./msp432/pmap.d \
./msp432/pss.d \
./msp432/ref_a.d \
./msp432/reset.d \
./msp432/rtc_c.d \
./msp432/spi.d \
./msp432/sysctl.d \
./msp432/systick.d \
./msp432/timer32.d \
./msp432/timer_a.d \
./msp432/uart.d \
./msp432/wdt_a.d 

OBJS += \
./msp432/adc14.obj \
./msp432/aes256.obj \
./msp432/comp_e.obj \
./msp432/cpu.obj \
./msp432/crc32.obj \
./msp432/cs.obj \
./msp432/dma.obj \
./msp432/flash.obj \
./msp432/fpu.obj \
./msp432/gpio.obj \
./msp432/i2c.obj \
./msp432/interrupt.obj \
./msp432/mpu.obj \
./msp432/pcm.obj \
./msp432/pmap.obj \
./msp432/pss.obj \
./msp432/ref_a.obj \
./msp432/reset.obj \
./msp432/rtc_c.obj \
./msp432/spi.obj \
./msp432/sysctl.obj \
./msp432/systick.obj \
./msp432/timer32.obj \
./msp432/timer_a.obj \
./msp432/uart.obj \
./msp432/wdt_a.obj 

OBJS__QUOTED += \
"msp432\adc14.obj" \
"msp432\aes256.obj" \
"msp432\comp_e.obj" \
"msp432\cpu.obj" \
"msp432\crc32.obj" \
"msp432\cs.obj" \
"msp432\dma.obj" \
"msp432\flash.obj" \
"msp432\fpu.obj" \
"msp432\gpio.obj" \
"msp432\i2c.obj" \
"msp432\interrupt.obj" \
"msp432\mpu.obj" \
"msp432\pcm.obj" \
"msp432\pmap.obj" \
"msp432\pss.obj" \
"msp432\ref_a.obj" \
"msp432\reset.obj" \
"msp432\rtc_c.obj" \
"msp432\spi.obj" \
"msp432\sysctl.obj" \
"msp432\systick.obj" \
"msp432\timer32.obj" \
"msp432\timer_a.obj" \
"msp432\uart.obj" \
"msp432\wdt_a.obj" 

C_DEPS__QUOTED += \
"msp432\adc14.d" \
"msp432\aes256.d" \
"msp432\comp_e.d" \
"msp432\cpu.d" \
"msp432\crc32.d" \
"msp432\cs.d" \
"msp432\dma.d" \
"msp432\flash.d" \
"msp432\fpu.d" \
"msp432\gpio.d" \
"msp432\i2c.d" \
"msp432\interrupt.d" \
"msp432\mpu.d" \
"msp432\pcm.d" \
"msp432\pmap.d" \
"msp432\pss.d" \
"msp432\ref_a.d" \
"msp432\reset.d" \
"msp432\rtc_c.d" \
"msp432\spi.d" \
"msp432\sysctl.d" \
"msp432\systick.d" \
"msp432\timer32.d" \
"msp432\timer_a.d" \
"msp432\uart.d" \
"msp432\wdt_a.d" 

C_SRCS__QUOTED += \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/adc14.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/aes256.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/comp_e.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/cpu.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/crc32.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/cs.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/dma.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/flash.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/fpu.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/gpio.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/i2c.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/interrupt.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/mpu.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/pcm.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/pmap.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/pss.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/ref_a.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/reset.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/rtc_c.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/spi.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/sysctl.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/systick.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/timer32.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/timer_a.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/uart.c" \
"D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432/wdt_a.c" 


