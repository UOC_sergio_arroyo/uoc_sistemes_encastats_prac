################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../msp432p401r.cmd 

C_SRCS += \
../acelerometer_driver.c \
../adc14_multiple_channel_no_repeat.c \
../general_functions.c \
../i2c_driver.c \
../lcd_driver.c \
../main.c \
../msp432_startup_ccs.c \
../system_msp432p401r.c 

C_DEPS += \
./acelerometer_driver.d \
./adc14_multiple_channel_no_repeat.d \
./general_functions.d \
./i2c_driver.d \
./lcd_driver.d \
./main.d \
./msp432_startup_ccs.d \
./system_msp432p401r.d 

OBJS += \
./acelerometer_driver.obj \
./adc14_multiple_channel_no_repeat.obj \
./general_functions.obj \
./i2c_driver.obj \
./lcd_driver.obj \
./main.obj \
./msp432_startup_ccs.obj \
./system_msp432p401r.obj 

OBJS__QUOTED += \
"acelerometer_driver.obj" \
"adc14_multiple_channel_no_repeat.obj" \
"general_functions.obj" \
"i2c_driver.obj" \
"lcd_driver.obj" \
"main.obj" \
"msp432_startup_ccs.obj" \
"system_msp432p401r.obj" 

C_DEPS__QUOTED += \
"acelerometer_driver.d" \
"adc14_multiple_channel_no_repeat.d" \
"general_functions.d" \
"i2c_driver.d" \
"lcd_driver.d" \
"main.d" \
"msp432_startup_ccs.d" \
"system_msp432p401r.d" 

C_SRCS__QUOTED += \
"../acelerometer_driver.c" \
"../adc14_multiple_channel_no_repeat.c" \
"../general_functions.c" \
"../i2c_driver.c" \
"../lcd_driver.c" \
"../main.c" \
"../msp432_startup_ccs.c" \
"../system_msp432p401r.c" 


