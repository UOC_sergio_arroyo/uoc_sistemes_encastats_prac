################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
GrLib/grlib/button.obj: ../GrLib/grlib/button.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="GrLib/grlib/button.d" --obj_directory="GrLib/grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

GrLib/grlib/checkbox.obj: ../GrLib/grlib/checkbox.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="GrLib/grlib/checkbox.d" --obj_directory="GrLib/grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

GrLib/grlib/circle.obj: ../GrLib/grlib/circle.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="GrLib/grlib/circle.d" --obj_directory="GrLib/grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

GrLib/grlib/context.obj: ../GrLib/grlib/context.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="GrLib/grlib/context.d" --obj_directory="GrLib/grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

GrLib/grlib/display.obj: ../GrLib/grlib/display.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="GrLib/grlib/display.d" --obj_directory="GrLib/grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

GrLib/grlib/image.obj: ../GrLib/grlib/image.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="GrLib/grlib/image.d" --obj_directory="GrLib/grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

GrLib/grlib/imageButton.obj: ../GrLib/grlib/imageButton.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="GrLib/grlib/imageButton.d" --obj_directory="GrLib/grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

GrLib/grlib/line.obj: ../GrLib/grlib/line.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="GrLib/grlib/line.d" --obj_directory="GrLib/grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

GrLib/grlib/radioButton.obj: ../GrLib/grlib/radioButton.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="GrLib/grlib/radioButton.d" --obj_directory="GrLib/grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

GrLib/grlib/rectangle.obj: ../GrLib/grlib/rectangle.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="GrLib/grlib/rectangle.d" --obj_directory="GrLib/grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

GrLib/grlib/string.obj: ../GrLib/grlib/string.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="GrLib/grlib/string.d" --obj_directory="GrLib/grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


