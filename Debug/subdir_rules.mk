################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
acelerometer_driver.obj: ../acelerometer_driver.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="acelerometer_driver.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

adc14_multiple_channel_no_repeat.obj: ../adc14_multiple_channel_no_repeat.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="adc14_multiple_channel_no_repeat.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

general_functions.obj: ../general_functions.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="general_functions.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

i2c_driver.obj: ../i2c_driver.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="i2c_driver.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

lcd_driver.obj: ../lcd_driver.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="lcd_driver.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

main.obj: ../main.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="main.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

msp432_startup_ccs.obj: ../msp432_startup_ccs.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="msp432_startup_ccs.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

system_msp432p401r.obj: ../system_msp432p401r.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/inc" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/fonts" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/GrLib/grlib" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/LcdDriver" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80/sensors" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/freertos/cortex-m4" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/Libraries/ti/msp432" --include_path="D:/UOC_SOFTWARE/Sistemes Encastats/workspace_v7/prac_saa80" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccs720/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccs720/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="system_msp432p401r.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


