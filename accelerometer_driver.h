/*
 * accelerometer_driver.h
 *
 *  Created on: 26 oct. 2017
 *      Author: toni
 */

#ifndef ACCELEROMETER_DRIVER_H_
#define ACCELEROMETER_DRIVER_H_

#define CONVERSION_SCALE             3250.0
#define CONVERSION_OFFSET            8150.0
#define ADC_CONV_VACC                16383

#define DIFF_CALIBRATE_0s            -0.14546
#define DIFF_CALIBRATE_0v            -0.022957



void init_Accel(void);

void Accel_read(float *values, float actualTemp);


#endif /* ACCELEROMETER_DRIVER_H_ */
