/*
 * acelerometer_driver.c
 *
 *  Created on: 26 oct. 2017
 *      Author: toni
 */

// Includes standard
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "accelerometer_driver.h"
#include "adc14_multiple_channel_no_repeat.h"


void init_Accel(void){
    init_ADC();
}

void Accel_read(float *values, float actualTemp)
{
    uint16_t *Data;
    uint8_t i;
    float desv_offset = actualTemp * 0.0007;
    float desv_offset_z = actualTemp * 0.0004;
    float v;

    // Solicita datos a ADC
    Data = ADC_read();

    // Realizar conversion con temperatura
    for (i=0; i<NUM_ADC_CHANNELS; i++) {
        v = (3.3 * Data[i]) / ADC_CONV_VACC;
        if (i == 2) {
            values[i] = ((v-1.65+DIFF_CALIBRATE_0s) / (0.66*(1+((0.04+DIFF_CALIBRATE_0s)*actualTemp)))) + desv_offset_z;

        } else {
            values[i] = ((v-1.65+DIFF_CALIBRATE_0v) / (0.66*(1+((0.01+DIFF_CALIBRATE_0s)*actualTemp)))) + desv_offset;
        }
    }
}
